﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NBandChecker;
using System.IO;

class Global : Singleton<Global>
{
    public static Form1 uiController;

    public static List<BandInfo> listBandInfo = new List<BandInfo>();
    public static List<ReplyInfo> listReply = new List<ReplyInfo>();
    public static List<FeedInfo> listFeed = new List<FeedInfo>();

    public static List<string> listTargetBandForFeed = new List<string>();

    public static List<FileInfo> adFiles = new List<FileInfo>();
    public static List<List<string>> listAdInfo = new List<List<string>>();
    public static int adFileIndex = 0;

    public static List<FileInfo> feedFiles = new List<FileInfo>();
    public static List<List<string>> listFeedInfo = new List<List<string>>();
    public static int feedFileIndex = 0;

    public static bool stop = false;

    public static string label_AnchorFeed;
    public static bool checkBox_Feed = false;

    public static void LoadAdInfo()
    {
        adFiles = Util.GetFileList("ReplyFiles", "txt");
        foreach (FileInfo item in adFiles)
        {
            List<string> fileInfo = new List<string>();
            Util.LoadTxtFile(item.Name, ref fileInfo, "ReplyFiles");
            listAdInfo.Add(fileInfo);
        }
    }

    public static List<string> GetAdInfo(ref string title_)
    {
        if (listAdInfo.Count <= 0)
            return null;

        if (adFileIndex >= listAdInfo.Count)
            adFileIndex = 0;

        title_ = adFiles[adFileIndex].Name;
        List<string> fileInfo = listAdInfo[adFileIndex];
        adFileIndex++;

        return fileInfo;
    }

    public static void LoadFeedInfo()
    {
        feedFiles = Util.GetFileList("FeedFiles", "txt");
        foreach (FileInfo item in feedFiles)
        {
            List<string> fileInfo = new List<string>();
            Util.LoadTxtFile(item.Name, ref fileInfo, "FeedFiles");
            listFeedInfo.Add(fileInfo);
        }
    }

    public static List<string> GetFeedInfo(ref string title_)
    {
        if (listFeedInfo.Count <= 0)
            return null;

        if (feedFileIndex >= listFeedInfo.Count)
            feedFileIndex = 0;

        title_ = feedFiles[feedFileIndex].Name;
        List<string> fileInfo = listFeedInfo[feedFileIndex];
        feedFileIndex++;

        return fileInfo;
    }
}

