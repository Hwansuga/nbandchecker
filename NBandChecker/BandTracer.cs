﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using OpenQA.Selenium;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.Threading;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using System.Net.Http;

namespace NBandChecker
{
    class BandTracer : DriverController
    {
        WebClient wc = new WebClient();
        HttpClient hc = null;
        HtmlDocument doc = new HtmlDocument();

        public string ua = string.Empty;
        public string secretKey = string.Empty;

        public Form1 uiController = null;

        string loginUrl = "https://auth.band.us/login_page";
        string feedUrl = "https://band.us/feed";

        public BandTracer()
        {
            CreatDriver(TYPE_DRIVER.TYPE_Chrome);
        }

        public void GoLogin()
        {
            driver.Navigate().GoToUrl(loginUrl);

            wc.Headers.Clear();

            wc.Headers.Add("Cache-Control", "no-cache");
            wc.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)");
            wc.Headers.Add("Content-Type", "application / zip, application / octet - stream");
            wc.Headers.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        }

        public void SetHeader(WebClient wc_, string key_, string value_)
        {
            if (wc_.Headers.AllKeys.Contains(key_))
                wc_.Headers[key_] = value_;
            else
                wc_.Headers.Add(key_, value_);

            if (hc != null)
            {
                hc.DefaultRequestHeaders.Add(key_, value_);
            }
        }

        public void CollectCookie()
        {
            if (driver == null)
                return;

            ua = (string)driver.ExecuteScript("return navigator.userAgent;");

            var cookieContainer = new CookieContainer();
            var myCookies = driver.Manage().Cookies.AllCookies;
            List<string> listCookies = new List<string>();
            foreach (OpenQA.Selenium.Cookie itemCookie in myCookies)
            {
                //wc.Headers.Add(HttpRequestHeader.Cookie ,$"{itemCookie.Name}={itemCookie.Value}");
                listCookies.Add($"{itemCookie.Name}={itemCookie.Value}");
                cookieContainer.Add(new Uri("https://band.us"), new System.Net.Cookie(itemCookie.Name, itemCookie.Value));
            }

            wc.Headers.Clear();
            wc.Headers.Add(HttpRequestHeader.Cookie, string.Join(";", listCookies));
//             
//             var handler = new HttpClientHandler() { CookieContainer = cookieContainer };
//             hc = new HttpClient(handler);

            Thread.Sleep(100);
        }

        public void GetSecretKey()
        {
            SetHeader(wc, "user-agent", this.ua);

            var nvc = new System.Collections.Specialized.NameValueCollection();
            var ts = Util.JavaScript_GetTime();
            nvc.Add("_t", $"{ts}");
            nvc.Add("callback", $"authCallBack_{ts}");
            nvc.Add("_", $"{Util.JavaScript_GetTime()}");

            var targetUrl = "https://auth.band.us/s/login/getKey" + Util.ToQueryString(nvc);
            var ret = wc.DownloadData(targetUrl);
            var retString = Encoding.UTF8.GetString(ret);

            var lines = retString.Replace("\r", "").Split('\n').ToList();
            var targetLine = lines.Find(x => x.Contains("secretKey: "));
            secretKey = targetLine.Replace("secretKey:", "").Replace("'", "").Trim();
        }

        public void CollectBandList()
        {
            SetHeader(wc, "user-agent", this.ua);


            var nvc = new System.Collections.Specialized.NameValueCollection();
            nvc.Add("ts",$"{Util.JavaScript_GetTime()}");
            nvc.Add("fields", $"band.band_no,band.name,band.cover,band.member_count,band.theme_color,band.type");
            nvc.Add("include_restricted", $"false");

            var targetUrl = "https://api.band.us/v2.0.1/get_band_list_with_filter" + Util.ToQueryString(nvc);

            //var request = new HttpRequestMessage(HttpMethod.Options, targetUrl);
//             var result = hc.SendAsync(request).Result;
//             var contentData = result.Content.ReadAsStringAsync().Result;
//             result = hc.GetAsync(targetUrl).Result;
//             contentData = result.Content.ReadAsStringAsync().Result;


            var ret = wc.DownloadData(targetUrl);
            var retString = Encoding.UTF8.GetString(ret);
            var jObj = JObject.Parse(retString);

            Global.listBandInfo.Clear();
            foreach (var jItem in jObj["result_data"] as JArray)
            {
                BandInfo data = new BandInfo
                {
                    BTrace = true,
                    BandName = jItem["band"]["name"].ToObject<string>(),
                    BandUrl = $"https://band.us/band/{jItem["band"]["band_no"].ToObject<string>()}"
                };

                Global.listBandInfo.Add(data);
            }
        }

        public void CollectAnchorFeedInfo()
        {
            try
            {
                IList<IWebElement> listFeed = driver.FindElements(By.ClassName("feedRecommendWrap"));
                if (listFeed.Count > 0)
                {
                    string feedInfo = listFeed.First().FindElement(By.XPath("./div/section/div/a")).GetAttribute("href");
                    Global.label_AnchorFeed = feedInfo;
                    Global.uiController.PrintLog("기준 Feed : " + feedInfo);
                }
            }
            catch
            {

            }
        }

        public bool CheckNewFeedCnt()
        {
            try
            {
                

                IWebElement feedCnd = driver.FindElementByClassName("_feedCount");
                if (feedCnd.Displayed)
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        bool WriteReply(IWebElement item_ , string msg_)
        {
            IWebElement postElem = item_.FindElement(By.ClassName("postAddBox"));
            if (postElem.GetAttribute("innerHTML").Contains("댓글쓰기"))
            {
                IList<IWebElement> listItem = postElem.FindElements(By.ClassName("addCol"));
                foreach(var item in listItem)
                {
                    if (item.Text.Contains("댓글쓰기"))
                    {
                        item.Click();
                        Thread.Sleep(2000);

                        IWebElement mention = item_.FindElement(By.ClassName("uInputComment"));
                        IWebElement typeArea = mention.FindElement(By.XPath("./div/textarea"));

                        typeArea.SendKeys(msg_);
                        IWebElement btn = item_.FindElement(By.ClassName("submitWrap"));
                        btn.Click();

                        string alert = IsAlertPresent();
                        if (string.IsNullOrEmpty(alert) == false)
                            Global.uiController.PrintLog(alert);

                        return true;
                    }
                }
            }
            else
            {
                Global.uiController.PrintLog("댓글 달기 옵션이 없음");
            }

            return false;
        }

        public void WorkWriteReply()
        {
            string anchorFeed = "";
            try
            {
                driver.Navigate().GoToUrl(feedUrl);
                Thread.Sleep(5000);

                WaitUntilExist(By.ClassName("feedRecommendWrap"));

                IList<IWebElement> listFeed = driver.FindElements(By.ClassName("feedRecommendWrap"));
                foreach(var tiem in listFeed)
                {                  
                    string feedInfo = tiem.FindElement(By.XPath("./div/section/div/a")).GetAttribute("href");
                    if (feedInfo == Global.label_AnchorFeed)                    
                        break;

                    if (tiem == listFeed.First())
                        anchorFeed = feedInfo;

                    BandInfo allowBand = Global.listBandInfo.Find(x => x.BTrace && feedInfo.Contains(x.BandUrl));
                    if (allowBand == null)
                    {
                        
                    }
                    else
                    {
                        string title = "";
                        List<string> msg = Global.GetAdInfo(ref title);

                        Global.uiController.PrintLog(feedInfo + " / 댓글 : " + title);

                        bool ret = WriteReply(tiem , string.Join("", msg.ToArray()));
                        if (ret)
                        {
                            Global.listReply.Add(new ReplyInfo { BandUrl = feedInfo, ReplyMsg = title });
                        }

                        //이미 포함되어 있다면 추가할 필요가 없음
                        if (Global.listTargetBandForFeed.Contains(allowBand.BandUrl) == false)
                            Global.listTargetBandForFeed.Add(allowBand.BandUrl);
                    }
                }

                Global.uiController.PrintLog("댓글 완료");
                if (string.IsNullOrEmpty(anchorFeed) == false)
                {
                    Global.label_AnchorFeed = anchorFeed;
                    Global.uiController.PrintLog("기준 Feed : " + anchorFeed);
                }                 
            }
            catch
            {
#if PRINT_LOG
                Global.uiController.PrintLog("Reply중 예외 발생");
#endif

                if (string.IsNullOrEmpty(anchorFeed) == false)
                {
                    Global.label_AnchorFeed = anchorFeed;
                    Global.uiController.PrintLog("기준 Feed : " + anchorFeed);
                }
            }
        }

        public void WriteFeed(string url_)
        {
            driver.Navigate().GoToUrl(url_);
            Thread.Sleep(3000);


            if (driver.PageSource.Contains("DPostFakeEditorView"))
            {
                string title = "";
                List<string> listFeed = Global.GetFeedInfo(ref title);

                IWebElement writeWrab = driver.FindElementByClassName("writeWrap");
                IWebElement writeBtn = writeWrab.FindElement(By.XPath("./div/button"));
                writeBtn.Click();

                Thread.Sleep(1000);

                //*[@id="wrap"]/div[2]/div/div/section/div/div/div
                //*[@id="wrap"]/div[2]/div/div/section/div/div/div/div[2]/div
                //*[@id="wrap"]/div[2]/div/div/section/div/div/div/div[3]/div/div/button
                IWebElement postRoot = driver.FindElementByClassName("cPostWrite");

                IWebElement writeArea = driver.FindElementByXPath("//*[@id='wrap']/div[2]/div/div/section/div/div/div/div[2]/div");
                writeArea.SendKeys(string.Join("", listFeed.ToArray()));

                Thread.Sleep(1000);

                //IWebElement btnAdd = postRoot.FindElement(By.XPath("div[3]/div/div/button"));
                IWebElement btnAdd = driver.FindElementByXPath("//*[@id='wrap']/div[2]/div/div/section/div/div/div/div[3]/div/div/button");
                btnAdd.Click();

                string alert = IsAlertPresent();
                if (string.IsNullOrEmpty(alert) == false)
                    Global.uiController.PrintLog(alert);

                Thread.Sleep(1000);

                Global.listFeed.Add(new FeedInfo() { BandUrl = url_, FeedMsg = title });
            }
            else
            {
                Global.uiController.PrintLog("포스팅 권한이 없는 밴드입니다 - " + url_);
            }
        }

    }
}
