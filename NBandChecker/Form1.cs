﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NBandChecker
{
    public partial class Form1 : Form
    {
        BandTracer dataScraper;

        BandTracer feedTracer;

        public Form1()
        {
            InitializeComponent();
        }

        public void PrintLog(string msg_)
        {
            listBox_Log.Invoke(new MethodInvoker(delegate ()
            {
                Util.AutoLineStringAdd(listBox_Log, msg_);
            }));
        }

        private void button_CreateWnd_Click(object sender, EventArgs e)
        {
            if (dataScraper != null)
            {
                dataScraper.QuiteDriver();
                dataScraper = null;
            }

            dataScraper = new BandTracer();
            dataScraper.uiController = this;
            dataScraper.GoLogin();

            if (Global.checkBox_Feed)
            {
                if (feedTracer != null)
                {
                    feedTracer.QuiteDriver();
                    feedTracer = null;
                }

                feedTracer = new BandTracer();
                feedTracer.uiController = this;
                feedTracer.GoLogin();
            }

            checkBox_Feed.Enabled = false;
           
            PrintLog("로그인 해주세요");
            PrintLog("로그인 완료 후 '밴드 가입정보 수집 버튼'을 눌러주세요");           
        }

        

        private void button_CollectData_Click(object sender, EventArgs e)
        {
            dataScraper.CollectCookie();
            dataScraper.GetSecretKey();
            dataScraper.CollectBandList();
            dataScraper.CollectAnchorFeedInfo();
            
            UpdateLabelUI();
            
            dataGridView_ListBand.AutoGenerateColumns = true;
            dataGridView_ListBand.DataSource = null;
            dataGridView_ListBand.DataSource = Global.listBandInfo;
            dataGridView_ListBand.Refresh();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
#if DEBUG
            Util.KillProcess("chromedriver");
#else
            Util.KillProcess("chrome");
#endif


            EventUtil.ConnectCustomEvent(this);
            Global.uiController = this;

            Global.LoadAdInfo();
            Global.LoadFeedInfo();
            UpdateListUI();


            dataGridView_ListReply.AutoGenerateColumns = true;

            UpdateReplyListUI();
        }

        public void UpdateLabelUI()
        {
            label_AnchorFeed.Invoke(new MethodInvoker(delegate() {
                label_AnchorFeed.Text = Global.label_AnchorFeed;
            }));
        }

        public void UpdateListUI()
        {
            this.Invoke(new MethodInvoker(delegate ()
            {
                listBox_ListReply.Items.Clear();
                foreach (FileInfo item in Global.adFiles)
                {
                    Util.AutoLineStringAdd(listBox_ListReply, item.Name, false);
                }

                listBox_ListFeed.Items.Clear();
                foreach (FileInfo item in Global.feedFiles)
                {
                    Util.AutoLineStringAdd(listBox_ListFeed, item.Name, false);
                }
            }));
        }

        public void UpdateReplyListUI()
        {
            dataGridView_ListReply.Invoke(new MethodInvoker(delegate () {
                dataGridView_ListReply.DataSource = null;
                dataGridView_ListReply.DataSource = Global.listReply;
                dataGridView_ListReply.Refresh();
            }));
        }

        private void button_Start_Click(object sender, EventArgs e)
        {
            Global.stop = false;

            button_CreateWnd.Enabled = false;
            button_CollectData.Enabled = false;
            button_Start.Enabled = false;
            checkBox_Feed.Enabled = false;

            PrintLog("시작");

            Thread worker = new Thread(WorkThread);
            worker.Start();

            Thread alertMonitor = new Thread(AlertCheckThread);
            alertMonitor.Start();

            if (Global.checkBox_Feed)
            {
                Thread worker2 = new Thread(FeedWordThread);
                worker2.Start();
            }            
        }

        void WorkThread()
        {
            while(Global.stop == false)
            {
                if (dataScraper.CheckNewFeedCnt())
                {
                    PrintLog("새 피드 감지");

                    dataScraper.WorkWriteReply();

                    UpdateLabelUI();
                    UpdateReplyListUI();
                }

                Thread.Sleep(1000);
            }

            StopDoneEvnet();
            PrintLog("완료");
        }

        void AlertCheckThread()
        {
            while (Global.stop == false)
            {
                if (dataScraper != null)
                {
                    string alter = dataScraper.IsAlertPresent();
                    if (string.IsNullOrEmpty(alter) == false)
                        Global.uiController.PrintLog(alter);
                }
                
                Thread.Sleep(5000);
            }
        }

        private void button_Stop_Click(object sender, EventArgs e)
        {
            Global.stop = true;
            PrintLog("정지중...");
        }

        public void StopDoneEvnet()
        {
            this.Invoke(new MethodInvoker(delegate () {
                button_CreateWnd.Enabled = true;
                button_CollectData.Enabled = true;
                button_Start.Enabled = true;
                checkBox_Feed.Enabled = true;
            }));
                        
        }

        public void UpdateFeedListUI()
        {
            dataGridView_ListFeed.Invoke(new MethodInvoker(delegate ()
            {
                dataGridView_ListFeed.DataSource = null;
                dataGridView_ListFeed.DataSource = Global.listFeed;
                dataGridView_ListFeed.Refresh();
            }));
        }

        public void FeedWordThread()
        {
            while(Global.stop == false)
            {
                if (Global.listTargetBandForFeed.Count > 0)
                {
//                     if (feedTracer == null)
//                     {
//                         feedTracer = new BandTracer();
//                         //feedTracer.GetCookieFromDriver(dataScraper);
//                     }

                    string url = Global.listTargetBandForFeed[0];
                    Global.listTargetBandForFeed.RemoveAt(0);

                    feedTracer.WriteFeed(url);

                    UpdateFeedListUI();
                }

                Thread.Sleep(10000);
            }
        }
    }
}
