﻿namespace NBandChecker
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button_CreateWnd = new System.Windows.Forms.Button();
            this.button_CollectData = new System.Windows.Forms.Button();
            this.listBox_Log = new System.Windows.Forms.ListBox();
            this.dataGridView_ListBand = new System.Windows.Forms.DataGridView();
            this.button_Start = new System.Windows.Forms.Button();
            this.button_Stop = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label_AnchorFeed = new System.Windows.Forms.Label();
            this.listBox_ListReply = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dataGridView_ListReply = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.listBox_ListFeed = new System.Windows.Forms.ListBox();
            this.dataGridView_ListFeed = new System.Windows.Forms.DataGridView();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.checkBox_Feed = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_ListBand)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_ListReply)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_ListFeed)).BeginInit();
            this.SuspendLayout();
            // 
            // button_CreateWnd
            // 
            this.button_CreateWnd.Location = new System.Drawing.Point(13, 13);
            this.button_CreateWnd.Name = "button_CreateWnd";
            this.button_CreateWnd.Size = new System.Drawing.Size(75, 23);
            this.button_CreateWnd.TabIndex = 0;
            this.button_CreateWnd.Text = "창 생성";
            this.button_CreateWnd.UseVisualStyleBackColor = true;
            this.button_CreateWnd.Click += new System.EventHandler(this.button_CreateWnd_Click);
            // 
            // button_CollectData
            // 
            this.button_CollectData.Location = new System.Drawing.Point(94, 13);
            this.button_CollectData.Name = "button_CollectData";
            this.button_CollectData.Size = new System.Drawing.Size(123, 23);
            this.button_CollectData.TabIndex = 1;
            this.button_CollectData.Text = "밴드 가입정보 수집";
            this.button_CollectData.UseVisualStyleBackColor = true;
            this.button_CollectData.Click += new System.EventHandler(this.button_CollectData_Click);
            // 
            // listBox_Log
            // 
            this.listBox_Log.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.listBox_Log.FormattingEnabled = true;
            this.listBox_Log.ItemHeight = 12;
            this.listBox_Log.Location = new System.Drawing.Point(12, 531);
            this.listBox_Log.Name = "listBox_Log";
            this.listBox_Log.Size = new System.Drawing.Size(620, 136);
            this.listBox_Log.TabIndex = 3;
            // 
            // dataGridView_ListBand
            // 
            this.dataGridView_ListBand.AllowUserToAddRows = false;
            this.dataGridView_ListBand.AllowUserToDeleteRows = false;
            this.dataGridView_ListBand.AllowUserToResizeColumns = false;
            this.dataGridView_ListBand.AllowUserToResizeRows = false;
            this.dataGridView_ListBand.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView_ListBand.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView_ListBand.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_ListBand.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dataGridView_ListBand.Location = new System.Drawing.Point(13, 42);
            this.dataGridView_ListBand.Name = "dataGridView_ListBand";
            this.dataGridView_ListBand.RowHeadersVisible = false;
            this.dataGridView_ListBand.RowTemplate.Height = 23;
            this.dataGridView_ListBand.Size = new System.Drawing.Size(619, 191);
            this.dataGridView_ListBand.TabIndex = 6;
            // 
            // button_Start
            // 
            this.button_Start.Location = new System.Drawing.Point(448, 13);
            this.button_Start.Name = "button_Start";
            this.button_Start.Size = new System.Drawing.Size(89, 23);
            this.button_Start.TabIndex = 7;
            this.button_Start.Text = "시작";
            this.button_Start.UseVisualStyleBackColor = true;
            this.button_Start.Click += new System.EventHandler(this.button_Start_Click);
            // 
            // button_Stop
            // 
            this.button_Stop.Location = new System.Drawing.Point(543, 13);
            this.button_Stop.Name = "button_Stop";
            this.button_Stop.Size = new System.Drawing.Size(89, 23);
            this.button_Stop.TabIndex = 8;
            this.button_Stop.Text = "정지";
            this.button_Stop.UseVisualStyleBackColor = true;
            this.button_Stop.Click += new System.EventHandler(this.button_Stop_Click);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 516);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 12);
            this.label1.TabIndex = 9;
            this.label1.Text = "기준 Feed : ";
            // 
            // label_AnchorFeed
            // 
            this.label_AnchorFeed.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label_AnchorFeed.AutoSize = true;
            this.label_AnchorFeed.Location = new System.Drawing.Point(91, 516);
            this.label_AnchorFeed.Name = "label_AnchorFeed";
            this.label_AnchorFeed.Size = new System.Drawing.Size(38, 12);
            this.label_AnchorFeed.TabIndex = 10;
            this.label_AnchorFeed.Text = "label2";
            // 
            // listBox_ListReply
            // 
            this.listBox_ListReply.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.listBox_ListReply.FormattingEnabled = true;
            this.listBox_ListReply.ItemHeight = 12;
            this.listBox_ListReply.Location = new System.Drawing.Point(13, 248);
            this.listBox_ListReply.Name = "listBox_ListReply";
            this.listBox_ListReply.Size = new System.Drawing.Size(150, 124);
            this.listBox_ListReply.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 236);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 12);
            this.label2.TabIndex = 13;
            this.label2.Text = "::댓글 파일 리스트::";
            // 
            // dataGridView_ListReply
            // 
            this.dataGridView_ListReply.AllowUserToAddRows = false;
            this.dataGridView_ListReply.AllowUserToDeleteRows = false;
            this.dataGridView_ListReply.AllowUserToResizeColumns = false;
            this.dataGridView_ListReply.AllowUserToResizeRows = false;
            this.dataGridView_ListReply.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView_ListReply.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView_ListReply.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_ListReply.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView_ListReply.Location = new System.Drawing.Point(171, 248);
            this.dataGridView_ListReply.Name = "dataGridView_ListReply";
            this.dataGridView_ListReply.ReadOnly = true;
            this.dataGridView_ListReply.RowHeadersVisible = false;
            this.dataGridView_ListReply.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridView_ListReply.RowTemplate.Height = 23;
            this.dataGridView_ListReply.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridView_ListReply.Size = new System.Drawing.Size(461, 124);
            this.dataGridView_ListReply.TabIndex = 14;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 375);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(117, 12);
            this.label3.TabIndex = 15;
            this.label3.Text = "::Feed 파일 리스트::";
            // 
            // listBox_ListFeed
            // 
            this.listBox_ListFeed.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.listBox_ListFeed.FormattingEnabled = true;
            this.listBox_ListFeed.ItemHeight = 12;
            this.listBox_ListFeed.Location = new System.Drawing.Point(13, 389);
            this.listBox_ListFeed.Name = "listBox_ListFeed";
            this.listBox_ListFeed.Size = new System.Drawing.Size(150, 124);
            this.listBox_ListFeed.TabIndex = 16;
            // 
            // dataGridView_ListFeed
            // 
            this.dataGridView_ListFeed.AllowUserToAddRows = false;
            this.dataGridView_ListFeed.AllowUserToDeleteRows = false;
            this.dataGridView_ListFeed.AllowUserToResizeColumns = false;
            this.dataGridView_ListFeed.AllowUserToResizeRows = false;
            this.dataGridView_ListFeed.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView_ListFeed.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView_ListFeed.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_ListFeed.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView_ListFeed.Location = new System.Drawing.Point(171, 389);
            this.dataGridView_ListFeed.Name = "dataGridView_ListFeed";
            this.dataGridView_ListFeed.ReadOnly = true;
            this.dataGridView_ListFeed.RowHeadersVisible = false;
            this.dataGridView_ListFeed.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridView_ListFeed.RowTemplate.Height = 23;
            this.dataGridView_ListFeed.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridView_ListFeed.Size = new System.Drawing.Size(461, 124);
            this.dataGridView_ListFeed.TabIndex = 17;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(169, 375);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 12);
            this.label4.TabIndex = 18;
            this.label4.Text = "::등록된 Feed::";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(169, 236);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 12);
            this.label5.TabIndex = 19;
            this.label5.Text = "::등록된 댓글::";
            // 
            // checkBox_Feed
            // 
            this.checkBox_Feed.AutoSize = true;
            this.checkBox_Feed.Location = new System.Drawing.Point(314, 20);
            this.checkBox_Feed.Name = "checkBox_Feed";
            this.checkBox_Feed.Size = new System.Drawing.Size(128, 16);
            this.checkBox_Feed.TabIndex = 20;
            this.checkBox_Feed.Text = "게시글 같이 올리기";
            this.checkBox_Feed.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(647, 678);
            this.Controls.Add(this.checkBox_Feed);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dataGridView_ListFeed);
            this.Controls.Add(this.listBox_ListFeed);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dataGridView_ListReply);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.listBox_ListReply);
            this.Controls.Add(this.label_AnchorFeed);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button_Stop);
            this.Controls.Add(this.button_Start);
            this.Controls.Add(this.dataGridView_ListBand);
            this.Controls.Add(this.listBox_Log);
            this.Controls.Add(this.button_CollectData);
            this.Controls.Add(this.button_CreateWnd);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "NBandChacker";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_ListBand)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_ListReply)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_ListFeed)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_CreateWnd;
        private System.Windows.Forms.Button button_CollectData;
        private System.Windows.Forms.ListBox listBox_Log;
        private System.Windows.Forms.DataGridView dataGridView_ListBand;
        private System.Windows.Forms.Button button_Start;
        private System.Windows.Forms.Button button_Stop;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label_AnchorFeed;
        private System.Windows.Forms.ListBox listBox_ListReply;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dataGridView_ListReply;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox listBox_ListFeed;
        private System.Windows.Forms.DataGridView dataGridView_ListFeed;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox checkBox_Feed;
    }
}

