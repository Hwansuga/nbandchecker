﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class BandInfo
{
    public bool BTrace { get; set; }
    public string BandName { get; set; }
    public string BandUrl { get; set; }
}

public class ReplyInfo
{
    public string BandUrl { get; set; }
    public string ReplyMsg { get; set; }
}

public class FeedInfo
{
    public string BandUrl { get; set; }
    public string FeedMsg { get; set; }
}

